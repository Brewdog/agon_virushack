package com.demid.hakaton.agon.json;

import com.vividsolutions.jts.geom.Coordinate;
import lombok.AllArgsConstructor;

import java.util.Map;

@AllArgsConstructor
public class TracksViewResponse {

    private Map<Coordinate,String> points;

}
