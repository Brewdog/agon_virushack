package com.demid.hakaton.agon.json;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class TracksViewRequest {
    private Double longitude;
    private Double latitude;
    private String time;
}
