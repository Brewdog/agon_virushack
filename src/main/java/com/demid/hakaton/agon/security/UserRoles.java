package com.demid.hakaton.agon.security;

public enum UserRoles {
    USER,
    ADMIN,
    GUEST
}