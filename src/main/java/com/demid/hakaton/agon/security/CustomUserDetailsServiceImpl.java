package com.demid.hakaton.agon.security;

import com.demid.hakaton.agon.entity.User;
import com.demid.hakaton.agon.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class CustomUserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository repo;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = repo.findByEmail(email);
        if (user == null){
            throw new UsernameNotFoundException("User with email: " + email + "not found");
        }
        return new CustomUserDetailsImpl(
                user.getId(),
                user.getEmail(),
                user.getPassword(),
                user.getUserRoles()
                        .stream()
                        .map(UserGrantedAuthorityImpl::new)
                        .collect(Collectors.toList())
        );
    }
}
