package com.demid.hakaton.agon.controller;

import com.demid.hakaton.agon.DTO.TrackDTO;
import com.demid.hakaton.agon.converter.ListTrackDtoToMapConverter;
import com.demid.hakaton.agon.json.TracksViewRequest;
import com.demid.hakaton.agon.json.TracksViewResponse;
import com.demid.hakaton.agon.service.TrackServiceImpl;
import com.vividsolutions.jts.geom.Coordinate;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@Data
@RequestMapping("/api")
@RequiredArgsConstructor
public class TrackController {
    private final TrackServiceImpl service;
    private final ListTrackDtoToMapConverter converter;

    @PostMapping("/getTracksByTime")
    ResponseEntity<TracksViewResponse> getTracksByTimeAndRadius(@RequestBody TracksViewRequest req) {
        Coordinate coordinate = new Coordinate(req.getLatitude(), req.getLongitude());
        LocalDateTime time = LocalDateTime.parse(req.getTime());
        List<TrackDTO> result = service.getListTracksByTime(coordinate, time);
        return ResponseEntity.ok(new TracksViewResponse(converter.convert(result)));
    }

}
