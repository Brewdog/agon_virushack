package com.demid.hakaton.agon.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SomeController {
    @GetMapping("/")
    ResponseEntity<?> testingResp(){
        return ResponseEntity.ok("Auth is alive");
    }
}

