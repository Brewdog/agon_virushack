package com.demid.hakaton.agon.DTO;

import com.vividsolutions.jts.geom.Coordinate;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class TrackDTO {
    private Long id;
    private Coordinate point;
    private LocalDateTime time;
}
