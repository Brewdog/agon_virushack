package com.demid.hakaton.agon.repository;

import com.demid.hakaton.agon.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

//    @Query("select u from User u where :email = u.email")
    User findByEmail(String email);
}
