package com.demid.hakaton.agon.service;

import com.demid.hakaton.agon.DTO.TrackDTO;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Point;

import java.time.LocalDateTime;
import java.util.List;

public interface TrackServiceCustom {
    List<TrackDTO> getListTracksByTime(Coordinate point, LocalDateTime time);

}
