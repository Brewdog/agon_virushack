package com.demid.hakaton.agon.service;

import com.demid.hakaton.agon.DTO.TrackDTO;
import com.vividsolutions.jts.geom.Coordinate;

import java.time.LocalDateTime;
import java.util.List;

public interface TrackService extends TrackServiceCustom {
    List<TrackDTO> getListTracksByTime(Coordinate point, LocalDateTime time);
}
