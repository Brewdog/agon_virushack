package com.demid.hakaton.agon.converter;

import com.demid.hakaton.agon.DTO.TrackDTO;
import com.vividsolutions.jts.geom.Coordinate;
import org.springframework.core.convert.converter.Converter;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ListTrackDtoToMapConverter implements Converter<List<TrackDTO>, Map<Coordinate,String>> {

    @Override
    public Map<Coordinate, String> convert(List<TrackDTO> trackDTOS) {

        return trackDTOS.stream()
                .collect(Collectors.toMap(TrackDTO::getPoint, x->x.getTime().toString()));
    }
}
