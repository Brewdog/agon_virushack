package com.demid.hakaton.agon.entity;

import com.demid.hakaton.agon.activityUtil.Load;
import com.demid.hakaton.agon.activityUtil.RateActivity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
public class Activity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime start;

    private LocalDateTime finish;

    @Enumerated(EnumType.STRING)
    private Load load;

    @Enumerated(EnumType.STRING)
    private RateActivity rate;

    @ManyToOne
    @JsonBackReference
    @ToString.Exclude
    private Account account;

}
