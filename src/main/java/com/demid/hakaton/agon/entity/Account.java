package com.demid.hakaton.agon.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long balance;

    @ManyToOne
    @JoinColumn(name = "holder_id")
    @JsonBackReference
    private User holder;

    @OneToMany
    private List<Activity> activities;
}
