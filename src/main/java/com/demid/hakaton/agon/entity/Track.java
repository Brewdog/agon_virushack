package com.demid.hakaton.agon.entity;


import com.vividsolutions.jts.geom.Coordinate;

import javax.persistence.*;

@Entity
public class Track {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Activity activity;

    @Column(name = "geom", nullable = false, columnDefinition = "geometry(Point,4326)")
    private Coordinate geom;
}
