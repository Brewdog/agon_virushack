package com.demid.hakaton.agon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SocialCitizenApplication {

    public static void main(String[] args) {
        SpringApplication.run(SocialCitizenApplication.class, args);
    }

}
